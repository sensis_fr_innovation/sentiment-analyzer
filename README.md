**How to setup**

1. Install MQTT https://mosquitto.org/download/
2. Install NPM

**How to run**

1. Go to project location
2. Execute "npm install"
3. Execute "npm start"
4. Access http://localhost:12000/viewer/loader.html

**Where to start? - feel free to trace and reverse engineer**

1. detection.js - contains the logic for facial detection
2. affdex.js - contains the logic for analyzing facial expressions
3. loader.html - UI structure
4. loader.js - UI behaviour
5. loader.css - UI styling