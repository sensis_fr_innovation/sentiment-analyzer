var raw_data;
var divRoot = $("#affdex_elements")[0];
var width = 640;
var height = 480;
var faceMode = affdex.FaceDetectorMode.LARGE_FACES;
var minAge = 0;
var maxAge = 0;
var gender = "NONE";
var dwell_seconds = 0;
var ticks = 0;
var ad_mode = "NORMAL";

//Construct a CameraDetector and specify the image width / height and face detector mode.
var detector;
//Enable detection of all Expressions, Emotions and Emojis classifiers.
$(document).ready(function() {
    log('#output', "Initializing emotion detector");
    detector = new affdex.CameraDetector(divRoot, width, height, faceMode);
    detector.detectAllAppearance();
    detector.detectAllEmotions();
    detector.detectAllEmojis();
    //Add a callback to notify when the detector is initialized and ready for runing.
    detector.addEventListener("onInitializeSuccess", function() {
        log('#output', "The detector reports initialized");
        //Display canvas instead of video feed because we want to draw the feature points on it
        $("#face_video").css("display", "none");
        $("#face_video_canvas").css("display", "block");
    });
    //Add a callback to notify when camera access is allowed
    detector.addEventListener("onWebcamConnectSuccess", function() {
        log('#output', "Webcam access allowed");
    });
    //Add a callback to notify when camera access is denied
    detector.addEventListener("onWebcamConnectFailure", function() {
        log('#output', "Webcam access denied");
    });
    detector.addEventListener("onStopSuccess", function() {
        log('#output', "DETECTION STOPPED");
    });
    var detectTimeout = setInterval(function() {
        dwell_seconds++;
        if (ticks > 3 && ad_mode != "NORMAL") {
            log("#output", "No one detected for sometime now...")
            $("#feature_gender").text("NONE");
            $("#feature_age").text("NONE");
            ad_mode = "NORMAL";
            ticks = 0;
        } else {
            ticks++;
            if(ad_mode != "NORMAL") {
                $("#dwell").text(dwell_seconds);
            } else {
                $("#dwell").text("N/A");
            }
        }
    }, 1000);
    detector.addEventListener("onImageResultsSuccess", function(faces, image, timestamp) {
        if (faces.length > 0) {
            var emotion_val_list = {};
            for (var property in faces[0].emotions) {
                if (faces[0].emotions.hasOwnProperty(property)) {
                    var id = "#" + property + "_meter";
                    var id_text = "#" + property + "_value";
                    var meter = $(id);
                    if (meter.length != 0) {
                        var emotion_val = Math.round(faces[0].emotions[property]);
                        emotion_val_list[property] = emotion_val;
                        // GET THE RAW DATA TO PROCESS LATER
                        raw_data = faces;
                        $("#emotion_log").text(JSON.stringify(faces));
                        //$(id_text).html(emotion_val);
                        $(id).attr("aria-valuenow", emotion_val);
                        $(id).css("width", emotion_val + "%");
                    }
                }
            }            
            ticks = 0;
            raw_data = faces;
            log('#results', "Result: " + JSON.stringify(faces[0]));
            for (var i = 0; i <= faces.length - 1; i++) {
                drawFeaturePoints(image, faces[i].featurePoints);
            };

            //$("#feature_age").text(faces[0].appearance.age);
            //$("#feature_ethnicity").text(faces[0].appearance.ethnicity);
            if (raw_data) {
                var age = faces[0].appearance.age.split(" ");
                //$("#feature_age").text(parseInt(age[0]) + " to " + parseInt(age[2]));
                if (age) {
                    if (parseInt(age[0]) != NaN && parseInt(age[0]) != "NaN") {
                        var minAge = parseInt(age[0]);
                        var maxAge = parseInt(age[2]);
                        var isMillenial = false;
                        var ageFlavorText0 = "Approximately "+minAge+" +/-";
                        if(ageFlavorText0 == "Approximately NaN +/-") {
                            ageFlavorText0 = "Still determining...";
                        }
                        var ageFlavorText = "FOREVER YOUNG";
                        if ((minAge+maxAge)/2 < 35) {
                            ageFlavorText = "MILLENIAL";
                            isMillenial = true;
                        }
                        //log("#output", "AGE: " + (parseInt(age[0]) + " to " + parseInt(age[2])));
                        if (gender != faces[0].appearance.gender) {
                            dwell_seconds = 0;
                            gender = faces[0].appearance.gender;
                            if (gender == "Female") {
                                $("#feature_gender").text("FEMALE");
                                $("#feature_age").text(age.join(" "));
                                log('#output', "FEMALE DETECTED");
                                ad_mode="F";
                            } else if (gender == "Male") {
                                $("#feature_gender").text("MALE");
                                $("#feature_age").text(age.join(" "));
                                log('#output', "MALE DETECTED");
                                ad_mode="M";
                            } else {
                                $("#feature_gender").text("NONE");
                                $("#feature_age").text("--");
                                log('#output', "NO ONE DETECTED");
                                ad_mode="NORMAL";
                            }
                        }
                    }
                }
                doSend({
                    "age": age.join(" "),
                    "gender": gender,
                    "emotions": emotion_val_list
                });                
            }
        }
    });
    onStart();
});

function log(node_name, msg) {
    if (null != writeToScreen) {
        writeToScreen(msg);
    }
    //$(node_name).append("<span>" + msg + "</span><br />")
}
//function executes when Start button is pushed.
function onStart() {
    if (detector && !detector.isRunning) {
        log('#output', "STARTING EMOTION DETECTOR");
        detector.start();
    }
}
//function executes when the Stop button is pushed.
function onStop() {
    log('#output', "STOPPING EMOTION DETECTOR");
    if (detector && detector.isRunning) {
        detector.removeEventListener();
        detector.stop();
        detector.reset();
    }
};
//Draw the detected facial feature points on the image
function drawFeaturePoints(img, featurePoints) {
    if($('#face_video_canvas').length > 0) {
        var contxt = $('#face_video_canvas')[0].getContext('2d');
        var hRatio = contxt.canvas.width / img.width;
        var vRatio = contxt.canvas.height / img.height;
        var ratio = Math.min(hRatio, vRatio);
        contxt.strokeStyle = "#FFFFFF";
        for (var id in featurePoints) {
            contxt.beginPath();
            contxt.arc(featurePoints[id].x,
                featurePoints[id].y, 2, 0, 2 * Math.PI);
            contxt.stroke();
        }
    }
}

var ad_open = false;
var current_ad = "NONE";

function switch_stats() {
    // YAW is attention
}

function start_logging() {

}

function stop_logging() {

}


$(function() {
    setInterval(function() {
    }, 3000);
});