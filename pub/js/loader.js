const canvas = document.querySelector('#main3d');
const canvas_width = canvas.clientWidth;
const canvas_height = canvas.clientHeight;
console.log("CANVAS_W:", canvas_width, ": CANVAS_H:", canvas_height);
// renderer + scene stuff
var scene = new THREE.Scene();
const renderer = new THREE.WebGLRenderer({
    canvas
});
//camera stuff
var camera_viewAngle = 45;
var camera_nearClipping = 0.1;
var camera_farClipping = 9999;
var camera_dist = 100;
var camera = new THREE.PerspectiveCamera(camera_viewAngle, canvas_width / canvas_height, camera_nearClipping, camera_farClipping);
var controls = new THREE.OrbitControls(camera);

function resizeRenderer(p_renderer) {
    var w = canvas.clientWidth;
    var h = canvas.clientHeight;
    console.log("CURRENT_CANVAS_W:", canvas_width, ": CURRENT_CANVAS_H:", canvas_height);
    var isResize = canvas.width !== w || canvas.height !== h;
    if (isResize) {
        p_renderer.setSize(w, h, false);
    }
    return isResize;
}
resizeRenderer(renderer);

var shoe;
var shoePivot;
var shoe_rotation = {
	"x": 0,
	"y": 90,
	"z": 0
};
function loadShoe() {
	const gltfLoader = new THREE.GLTFLoader();
    gltfLoader.load('shoe/scene.gltf', (gltf) => {
        shoe = gltf.scene;
    	shoePivot = new THREE.Object3D();
    	const pivotPt = new THREE.Vector3(0,0,0);
        shoe.position.x = -15;
        shoe.position.y = -40;
        shoe.position.z = 160;
        shoePivot.rotation.y = shoe_rotation.y * Math.PI / 180;
    	shoePivot.add(shoe);
        scene.add(shoePivot);
    });	
}

var cube;
var sphere;
var cube_rotation = 0;
var sphere_rotation = 0;
function addObjects() {
    var cubeGeometry = new THREE.BoxGeometry(20, 20, 20);
    var sphereGeometry = new THREE.SphereGeometry(60, 20, 20);
    var c_material = new THREE.MeshLambertMaterial({
        color: 0x00CC66,
        wireframe: true
    });
    var s_material = new THREE.MeshLambertMaterial({
        color: 0x888888,
        wireframe: true
    });    
    cube = new THREE.Mesh(cubeGeometry, c_material);
    sphere = new THREE.Mesh(sphereGeometry, s_material);
    //scene.add(cube);
    //scene.add(sphere);
}

function loadLights() {
    const skyColor = 0xB1E1FF;
    const groundColor = 0x555555;
    const h_intensity = 2.0;
    var h_light = new THREE.HemisphereLight(skyColor, groundColor, h_intensity);
    scene.add(h_light);
    var pTopR_light = new THREE.PointLight(0xFFFFFF,0.8);
    pTopR_light.position.set( 100, 0, 0 );
    scene.add(pTopR_light);
    var pTopL_light = new THREE.PointLight(0xFFFFFF,0.8);
    pTopL_light.position.set( -100, 0, 0 );
    scene.add(pTopL_light);    

}

function init() {
	addObjects();
	loadShoe();
	loadLights();
    scene.background = new THREE.Color(0x333333);
    camera.position.set(0, 0, camera_dist);
}
init();

function animate() {
	if(cube != null) {
		//cube.rotation.x = cube_rotation * Math.PI / 180;
	  	//cube.rotation.y = cube_rotation * Math.PI / 180;		
	  	//cube.rotation.z = cube_rotation * rotation * Math.PI / 180;
	}
  	if(sphere != null) {
  		//sphere.rotation.y = (sphere_rotation) * Math.PI / 180;  		
  	}
  	if(shoePivot != null) {  		
  		shoePivot.rotation.x = (shoe_rotation.x) * Math.PI / 180;
  		shoePivot.rotation.y = (shoe_rotation.y) * Math.PI / 180;
  		shoePivot.rotation.z = (shoe_rotation.z) * Math.PI / 180;
  	}
	if(cube_rotation >= 360) {
		cube_rotation=0;
	} else {
		cube_rotation += 1;		
	}  	
	if(sphere_rotation >= 360) {
		sphere_rotation=0;
	} else {
		sphere_rotation += 1;		
	}  	
	/*
	if(shoe_rotation >= 360) {
		shoe_rotation=0;
	} else {
		shoe_rotation += 2;		
	} 
	*/ 	
}

function camera_update() {
    //camera.position.set(0, 0, camera_dist);
    camera.aspect = canvas.clientWidth / canvas.clientHeight;
    camera.lookAt(scene.position);
    camera.updateProjectionMatrix();
    //controls.update();
}

function render() {
    camera_update();
    animate();
    requestAnimationFrame(render);
    renderer.render(scene, camera);
}
render();