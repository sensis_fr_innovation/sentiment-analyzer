var output;

function initLogger() {
	output = document.getElementById("output");
	writeToScreen("<i>logger initialized</i>");
}

function writeToScreen(message) {
	if (null != output) {
		var pre = document.createElement("div");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;
		output.appendChild(pre);
	}
}
window.addEventListener("load", initLogger, false);