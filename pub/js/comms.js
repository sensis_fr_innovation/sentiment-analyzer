var wsUri = "ws://localhost:12000/charsiu_ws";
var output;
var websocket;
var heading = "";

function init() {
	initWebSocket();
}

function initWebSocket() {
	websocket = new WebSocket(wsUri);
	websocket.onopen = function(evt) {
		writeToScreen("CONNECTED");
	};
	websocket.onclose = function(evt) {
		writeToScreen("DISCONNECTED");
	};
	websocket.onmessage = function(evt) {
		//writeToScreen('<span style="color: cyan;">RECEIVED: ' + evt.data + '</span>');
		var j_message = JSON.parse(evt.data);
		if(j_message.message == "TEL") {
			// PERFORM COMMAND HERE
		}
		//websocket.close();
	};
	websocket.onerror = function(evt) {
		writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
	};
}

var comms_interval;

function start_comms() {
	writeToScreen("ENABLED COMMS");
	comms_interval = setInterval(function() {
		// GET ANGLES HERE
	}, 100)
}

function stop_comms() {
	writeToScreen("DISABLED COMMS");
	clearInterval(comms_interval);
}

function doSend(message) {
	//writeToScreen("SENT: " + message);
	websocket.send(JSON.stringify(message));
}

window.addEventListener("load", init, false);