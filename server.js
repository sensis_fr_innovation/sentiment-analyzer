// CONSTANTS
const PORT = process.env.PORT = 12000;
const CONSOLE_OVERRIDE = true;
const EXIT_GO = true;
// OTHER includes
const logger = require('./logger');
const mqtt = require("./mqtt");
// SERVER STUFF
var WebSocket = require("ws");
var WebSocketServer = require("ws").Server;
var http = require("http");
const express = require('express');
var colors = require("colors");
const app = express();
server = http.createServer(app);
var wss = new WebSocketServer({
    server: server,
    path: "/charsiu_ws"
});
const t_topic = "face/telemetry";
app.use("/viewer", express.static('./pub'));
server.listen(PORT, () => {
    logger.log(colors.magenta('CHAR-SIU RUNNING:', PORT));
});
// Websocket
var mqtt_client = null;
wss.on("connection", function(ws) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    logger.log("WEBSOCKET CLIENT CONNECTED");
    mqtt_client = new mqtt.RawrMQTT({
        address: "127.0.0.1",
        topic: t_topic,
        onMessage: telemetryReceived
    });
    ws.on("message", (data) => {
        mqtt_client.publish(t_topic,"DAT",data);
    });
});

function telemetryReceived(j_message) {
    //logger.log(j_message);
    wss.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN) {
            client.send(JSON.stringify(j_message));
        }
    });
}

function noop() {}

function heartbeat() {
    this.isAlive = true;
}
const interval = setInterval(function ping() {
    wss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) {
            return ws.terminate();
        }
        ws.isAlive = false;
        ws.ping(noop);
    });
}, 30000);

function exitHandler(options, err) {
    logger.log("WARNING: ISSUED QUIT".underline.red);
    if (options.cleanup) {
        if (wss != null) {}
        logger.log("NORMAL: clean".bgGreen.white);
    }
    if (err && err != 'SIGINT') {
        logger.log("CRITICAL: ERROR".bgBlue.red);
        logger.log(err.stack);
    }
    if (options.exit) {
        logger.log("NORMAL: EXIT".bgGreen.white);
        if (EXIT_GO) {
            process.exit(0);
        }
    }
}
//do something when app is closing
process.on('exit', exitHandler.bind(null, {
    cleanup: true
}));
process.on('SIGINT', exitHandler.bind(null, {
    exit: true
}));
process.on('uncaughtException', exitHandler.bind(null, {
    exit: true
}));