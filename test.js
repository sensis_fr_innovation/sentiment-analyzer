var colors = require("colors");
// connect mqtt client
const t_topic="face/telemetry";
var mqtt = require('mqtt')
var hostname = "127.0.0.1";
var mqtt_client = mqtt.connect(hostname, {
    "protocol": 'mqtt',
    "hostname": hostname
});
mqtt_client.on('connect', function() {
    mqtt_client.subscribe(t_topic);
    console.log(colors.magenta("CONNECTED TO MQTT"));
});
mqtt_client.on('message', function(topic, message) {
    var j_message = JSON.parse(message);
    //console.log(j_message);
    if (j_message != null && j_message.type == "REQ") {
        console.log(colors.yellow("RECEIVED REQ"));
	    mqtt_client.publish(t_topic, JSON.stringify({
	        "type": "ACK"
	    }));        
    }
    if (j_message != null && j_message.type == "DAT") {
        console.log(colors.gray(JSON.stringify(j_message.data)));
    }
});
mqtt_client.on('error', function(error) {
    console.log(colors.red(error));
    mqtt_client.end();
});
mqtt_client.on('close', function(why) {
    console.log(colors.yellow(null == why ? "CLOSED: No reason" : why));
    mqtt_client.end();
})
/*
var pub = setTimeout(function() {
    mqtt_client.publish(t_topic, JSON.stringify({
        "message": "ACK"
    }));
    //process.exit(0);
    clearInterval(pub);
    mqtt_client.end();
}, 1000);
*/