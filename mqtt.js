const colors = require("colors");
const logger = require("./logger");
// connect mqtt client
const mqtt = require('mqtt');
class RawrMQTT {
    constructor(specs) {
        var that = this;
        this.address = specs.address;
        this.topic = specs.topic;
        this.mqtt_client = mqtt.connect(specs.address, {
            "protocol": 'mqtt'
        });
        var MQTT_ACK = {
            "MESSAGE": "OK"
        }
        var mqtt_ack = false;
        this.mqtt_client.on('connect', function() {
            that.mqtt_client.subscribe(that.topic);
            logger.log(colors.magenta("CONNECTED TO MQTT"));
            var mqtt_ack_i = setInterval(function() {
                if (mqtt_ack) {
                    logger.log(colors.cyan("ACK ACCEPTED"));
                    that.onMessage = specs.onMessage;
                    clearInterval(mqtt_ack_i);
                } else {
                    that.mqtt_client.publish(that.topic, JSON.stringify({
                        "type": "REQ"
                    }));
                }
            }, 1000);
        })
        this.mqtt_client.on('message', function(topic, message) {
            //console.log(message.toString());
            var j_message = JSON.parse(message);
            if (that.onMessage != null) {
                that.onMessage(j_message);
            } else {
                if (j_message != null && j_message.type == "ACK") {
                    logger.log(colors.yellow("RECEIVED ACK"));
                    mqtt_ack = true;
                }
            }
        });
        this.publish = (topic, type, message) => {
            this.mqtt_client.publish(topic, JSON.stringify({
                "type": "DAT",
                "data": message
            }));
        }
    }
    static typeOf(source) {
        return (source instanceof RawrMQTT);
    }
}
module.exports.RawrMQTT = RawrMQTT;
//END MQTT